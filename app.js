const express = require('express');
const morgan = require('morgan');
const app = express();

const tourRouter = require('./routes/tourRoutes');
const usersRouter = require('./routes/usersRoutes');


app.use('/api/v1/tours', tourRouter);
app.use('/api/v1/users', usersRouter);
app.use(express.static(`${__dirname}/public`));

app.use(express.json());
app.use(morgan('dev'));

app.use((req, res, next) => {
  req.requestTime = new Date().toISOString();
  next();
});

module.exports = app;

