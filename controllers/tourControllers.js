const fs = require('fs');

const tours = JSON.parse(fs.readFileSync(`${__dirname}/../dev-data/data/tours-simple.json`));

exports.checkID = (req, res, next, value) => {
  if (req.params.id * 1 > tours.length - 1) {
   return  res.status(404).json({
      status: 'fail',
      message: 'This id is not exist'
    });
  }
  next();
};

exports.getAllTours = (req, res) => {
  res.status(200).json({
      status: 'success',
      requestedAt: req.requestTime,
      results: tours.length,
      data: {
        tours
      }
    }
  );
};
exports.createNewTour = (req, res) => {
  const newID = tours[tours.length - 1].id + 1;
  const newTour = Object.assign({ id: newID }, req.body);

  tours.push(newTour);
  fs.writeFile(`${__dirname}/dev-data/data/tours-simple.json`, JSON.stringify(tours), err => {
    res.status(201).json({
      status: 'success',
      data: {
        tours: newTour
      }
    });
    if (err) {
      alert(`Error: ${err}`);
    }
  });
};
exports.getToursById = (req, res) => {
  const id = Number(req.params.id);

  if (id > tours.length - 1) {
    res.status(404).json({
      status: 'fail',
      message: 'This id is not exist'
    });
  }

  const tour = tours.find(el => el.id === id);
  res.status(200).json({
      status: 'success',
      data: {
        tours: tour
      }
    }
  );
};
exports.updateTourById = (req, res) => {
  if (req.params.id * 1 > tours.length - 1) {
    res.status(404).json({
      status: 'fail',
      message: 'This id is not exist'
    });
  }

  res.status(200).json({
    status: 'success',
    data: {
      tours: '<Update tours>'
    }
  });
};
exports.deleteTourById = (req, res) => {
  res.status(204).json({
    status: 'success',
    data: null
  });
};