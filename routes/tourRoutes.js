const express = require('express');

const router = express.Router();
const {
  getAllTours,
  createNewTour,
  getToursById,
  updateTourById,
  deleteTourById,
  checkID
} = require('../controllers/tourControllers');

router.param('id', checkID);

router.route('/')
  .get(getAllTours)
  .post( createNewTour);

router.route('/:id')
  .get(getToursById)
  .patch(updateTourById)
  .delete(deleteTourById);


module.exports = router;