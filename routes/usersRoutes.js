const express = require('express');

const router = express.Router();
const { getAllUsers, createUser, updateUserById, deleteUserById, getUserById } = require('../controllers/usersControllers');


router.route('/')
  .get(getAllUsers)
  .post(createUser);

router.route('/:id')
  .patch(updateUserById)
  .delete(deleteUserById)
  .get(getUserById);

module.exports = router;